<?php

/**
 * @file
 * Contains kvantstudio.page.inc.
 *
 * Template preprocess functions for kvantstudio.module.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_kvantstudio_personal_data_confirm(&$variables) {
  $config = \Drupal::config('kvantstudio.settings');
  $nid = (int) $config->get('node_cookie_policy');
  if (is_numeric($nid) && $nid) {
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
  } else {
    $uri = 'public://' . $nid;
    $alias = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
  }

  $text = t($config->get('text_cookie_policy'));
  $cookie_policy_information = str_replace("@site_name", $_SERVER['HTTP_HOST'], $text);
  $cookie_policy_information = str_replace("@cookie_policy_url", $alias, $cookie_policy_information);
  $variables['cookie_policy_information'] = $cookie_policy_information;
}
