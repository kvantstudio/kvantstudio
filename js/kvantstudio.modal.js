/**
 * @file
 * Misc JQuery scripts for control modal popups.
 */

(function ($, Drupal, settings) {
  'use strict';

  var ModalApp = {};

  ModalApp.ModalProcess = function (parameters) {
    if (typeof parameters['width'] != 'undefined') {
      this.width = ' style="max-width:' + parameters['width'] + 'px"';
    }
    this.width = ' style="max-width:' + parameters['width'] + 'px"' || ' style=""';
    this.id = parameters['id'] || 'modal';
    this.selector = parameters['selector'] || '';
    this.title = parameters['title'] || '';
    this.body = parameters['body'] || '';
    this.content =
      '<div class="modal fade" id="' +
      this.id +
      '" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog" role="document"' +
      this.width +
      '>' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<h5 class="modal-title">' +
      this.title +
      '</h5>' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
      '</div>' +
      '<div class="modal-body">' +
      this.body +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    this.init = function () {
      if ($('#' + this.id).length === 0) {
        $('body').prepend(this.content);
      }
      if (this.selector) {
        $(document).on('click', this.selector, $.proxy(this.showModal, this));
      }
    };
  };

  ModalApp.ModalProcess.prototype.changeWidth = function (content) {
    $('#' + this.id + ' .modal-dialog').attr('style', 'max-width:' + content);
  };

  ModalApp.ModalProcess.prototype.changeTitle = function (content) {
    $('#' + this.id + ' .modal-title').html(content);
  };

  ModalApp.ModalProcess.prototype.changeBody = function (content) {
    $('#' + this.id + ' .modal-body').html(content);
  };

  ModalApp.ModalProcess.prototype.showModal = function () {
    $('#' + this.id).modal('show');
  };

  ModalApp.ModalProcess.prototype.hideModal = function () {
    $('#' + this.id).modal('hide');
  };

  ModalApp.ModalProcess.prototype.updateModal = function () {
    $('#' + this.id).modal('handleUpdate');
  };

  // Инициализация модального окна.
  var formModal = new ModalApp.ModalProcess({
    id: 'modal',
    title: ''
  });
  formModal.init();

  Drupal.behaviors.loadModalFormBehavior = {
    attach: function (context, settings) {
      $('.form-load-modal:not(.processed)', context).addClass('processed').click(function () {
        var elem = null;
        var width = 'auto';

        if ((width = $(this).attr('data-form-width'))) {
          formModal.changeWidth(width + 'px');
        } else {
          formModal.changeWidth(width);
        }

        formModal.changeTitle($(this).attr('data-form-title'));

        // Если передан путь формы для загрузки из url.
        if ((elem = $(this).attr('data-form-path'))) {
          // Дополнительные параметры, которые нужно передать в форму.
          // Атрибут с параметрами передаваемыми в форму должен быть вида - data-form-parameters="type::event--subject::Регистрация на {{ item.title }}".
          // Знак :: разделяет параметр и его значение, знак -- разделяет несколько параметров.
          // Далее эта строка преобразуется в JSON объект и передается в качестве параметра в функцию вызова формы.
          var data_json = 'null';
          var data_object = {};
          var form_parameters = null;
          if ((form_parameters = $(this).attr('data-form-parameters'))) {
            var form_parameters_array = form_parameters.split('--');

            var element_array = [];
            form_parameters_array.forEach(function (element) {
              element_array = element.split('::');
              data_object[element_array[0]] = element_array[1];
            });

            data_json = JSON.stringify(data_object);
            if (!IsValidJSONString(data_json)) {
              data_json = 'null';
            }
          }

          var ajaxObject = Drupal.ajax({
            url: '/kvantstudio-form-load/' + elem + '/' + data_json + '/nojs',
            base: false,
            element: false,
            progress: false,
            cache: false
          });

          ajaxObject.execute().done(function (receivedHtml) {
            formModal.showModal();
            Drupal.attachBehaviors();
          });
        }

        // Если передан идентификатор элемента для загрузки в форму.
        if ((elem = $(this).attr('data-form-id'))) {
          var body = $(elem).html();
          formModal.changeBody(body);
          formModal.showModal();
        }
      });
    }
  };

  /**
   * Валидация JSON объекта.
   */
  function IsValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
})(jQuery, Drupal, drupalSettings);
