(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.getRoutePath = {
    attach: function (context, settings) {
      once('getRoutePath', 'html', context).forEach(function (element) {
        Drupal.getRoutePath = async function (routeName, getInternalPath = false, parameters = {}, options = {}) {
          let result = '';

          let url = Drupal.url('js-get-route-path/') + encodeURIComponent(routeName);
          let response = await fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'X-Requested-With': 'XMLHttpRequest'
            },
            body: JSON.stringify({ get_internal_path: getInternalPath, parameters: parameters, options: options })
          });

          if (response.ok) {
            let body = await response.json();
            result = body.path;
          }

          return result;
        };
      });
    }
  }
})(Drupal, once);
