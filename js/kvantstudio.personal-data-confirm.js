/**
 * @file
 * Misc JQuery scripts in this file
 */
(function (Drupal) {
  'use strict';

  Drupal.behaviors.personalData = {
    attach: function (context, settings) {
      var personalDataAgreementExist = localStorage.getItem('personalDataAgreement') || 0;
      if (!personalDataAgreementExist) {
        var item = document.getElementById('personal-data-confirm-wrapper');
        var display = getComputedStyle(item).display;
        if (display == 'none') {
          item.style.display = 'block';
        }
      }
    },
  };

  Drupal.behaviors.personalDataConfirm = {
    attach: function (context, settings) {
      [].forEach.call(context.querySelectorAll('.personal-data-confirm__button'), function (el) {
        el.addEventListener('click', function () {
          localStorage.setItem('personalDataAgreement', 1);
          var item = document.getElementById('personal-data-confirm-wrapper');
          item.style.display = 'none';
        });
      });
    },
  };
})(Drupal);
