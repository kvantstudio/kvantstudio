/**
 * @file
 * Implements various commonly used utility functions.
 */
(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.openLink = {
    attach: function (context, settings) {
      [].forEach.call(context.querySelectorAll('.js-open-link'), function(el) {
        el.addEventListener('click', function() {
          let url = this.getAttribute('data-link');
          window.open(url, '_self');
        })
      });
    }
  };

})(Drupal, drupalSettings);
