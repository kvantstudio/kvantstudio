(() => {

  'use strict';

  window.addEventListener('load', async (event) => {
    if (navigator.sendBeacon) {
      const data = JSON.stringify({
        event: event
      });
      navigator.sendBeacon('/js-page-load', data);
    }
  });

})();
