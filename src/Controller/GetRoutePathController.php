<?php

namespace Drupal\kvantstudio\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use \Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

/**
 * GetRoutePathController Class.
 */
class GetRoutePathController implements ContainerInjectionInterface {

  /**
   * The route provider to load routes by name.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The request stack variable.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  protected $requestStack;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\drupal_js_path\Controller\DrupalJsPath object.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider to load routes by name.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Symfony\Component\Routing\Generator\UrlGeneratorInterface $url_generator
   *   The URL generator.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(RouteProviderInterface $route_provider, RequestStack$request_stack, LoggerInterface $logger) {
    $this->routeProvider = $route_provider;
    $this->requestStack = $request_stack;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('request_stack'),
      $container->get('logger.channel.kvantstudio')
    );
  }

  /**
   * Returns the path of route.
   */
  public function getPathByRoute(string $route_name = ""): JsonResponse {
    $request = $this->requestStack->getCurrentRequest();

    if (!$request->isXmlHttpRequest()) {
      throw new BadRequestHttpException();
    }

    if (!$route_name) {
			throw new BadRequestHttpException();
		}

    $data = $request->getContent();
    if (!$data) {
			throw new BadRequestHttpException();
		}
		$data = Json::decode($data);

    $get_internal_path = (bool) ($data['get_internal_path'] ?? FALSE);
    $parameters = (array) ($data['parameters'] ?? []);
    $options = (array) ($data['options'] ?? []);

    $message = NULL;
    $path = NULL;
    try {
      if (!empty($this->routeProvider->getRoutesByNames([$route_name]))) {
        /** @var \Drupal\Core\Url $url */
        $url =  Url::fromRoute($route_name, $parameters, $options);

        $path = $url->toString();
        if ($get_internal_path) {
          $path = $url->getInternalPath();
        }
      }
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
      $message = $e->getMessage();
    }

    return new JsonResponse(['message' => $message, 'path' => $path]);
  }

}
