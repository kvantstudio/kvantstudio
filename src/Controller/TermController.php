<?php

namespace Drupal\kvantstudio\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\kvantstudio\Event\TermEvents;
use Drupal\kvantstudio\Event\TermPageBuildEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides custom controller for taxonomy routes.
 */
class TermController implements ContainerInjectionInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * The build process for term pages.
   *
   * @param \Drupal\taxonomy\TermInterface $entity
   *   The taxonomy term entity.
   *
   * @return array|null
   *   The result render array.
   */
  public function build(TermInterface $taxonomy_term) {
    // Check the access rights to view the entity as a page.
    $access = $taxonomy_term->access('view');
    if (!$access) {
      throw new NotFoundHttpException();
    }

    // Provides event which fires when prepare term page render array result.
    $event = new TermPageBuildEvent($taxonomy_term);
    $this->eventDispatcher->dispatch($event, TermEvents::PAGE_BUILD);
    $build = $event->getBuildArray();
    if (!empty($build)) {
      return $build;
    }

    return views_embed_view('taxonomy_term', 'page_1', $taxonomy_term->id());
  }

}
