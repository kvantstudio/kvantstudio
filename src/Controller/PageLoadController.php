<?php

namespace Drupal\kvantstudio\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\kvantstudio\Event\PageLoadEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PageLoadController implements ContainerInjectionInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * Dispatch event PAGE_LOAD.
   *
   * @param Request $request
   *   Request represents an HTTP request.
   * @param string $method
   *   Check ajax request.
   *
   * @return AjaxResponse
   */
  public function build(Request $request): AjaxResponse {
    $response = new AjaxResponse();

    // Validation of input data.
    $data = $request->getContent();
    if (!$data) {
      throw new BadRequestHttpException();
    }
    $data = Json::decode($data);

    // Dispatch event PAGE_LOAD.
    $event = new PageLoadEvent($request, $response, $data);
    $this->eventDispatcher->dispatch($event, PageLoadEvent::PAGE_LOAD);

    // Gets modified response.
    $response = $event->getResponse();

    return $response;
  }
}
