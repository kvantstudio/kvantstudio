<?php

namespace Drupal\kvantstudio\Controller;

use Drupal\Component\Utility\Html;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchNodeAutocomplete {

  /**
   * Поиск autocomplete по заголовку для сущностей node.
   */
  public function autocomplete(Request $request) {
    $string = $request->query->get('q');

    $matches = [];
    $matches_id = [];

    if ($string) {
      $string_array = explode(" ", $string);
      $request = '';
      $arguments = array();
      $i = 1;
      $count_array_string = count($string_array);
      foreach ($string_array as $key) {
        $arguments[':key' . $i] = $key;
        if ($count_array_string > $i) {
          $request .= 'INSTR(i.title, :key' . $i . ') > 0 AND ';
        } else {
          $request .= 'INSTR(i.title, :key' . $i . ') > 0';
        }
        $i++;
      }

      $query = \Drupal::database()->select('node_field_data', 'i');
      $query->fields('i', array('nid', 'type', 'title'));
      $query->where($request, $arguments);
      $query->orderBy('i.title', 'ASC');
      $query->range(0, 10);
      $result = $query->execute();

      foreach ($result as $row) {
        if (!array_key_exists($row->nid, $matches_id)) {
          $matches_id[$row->nid] = $row->nid;
          $value = Html::escape($row->title);
          $matches[] = ['value' => $row->nid, 'label' => 'nid:' . $row->nid . ':' . $row->type . ' — ' . $value];
        }
      }
    }

    return new JsonResponse($matches);
  }
}
