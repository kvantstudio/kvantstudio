<?php

namespace Drupal\kvantstudio\Controller;

use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\kvantstudio\Validator;

class FormLoadController extends ControllerBase {

  /**
   * Загружает данные в блок с выбранным идентификатором.
   *
   * @return ajax response
   *
   * @access public
   */
  public function formLoad($path, $parametrs, $method) {
    $response = new AjaxResponse();

    if ($method == 'ajax') {
      $route_name = Url::fromUserInput('/' . $path)->getRouteName();

      if ($route_name) {
        $route_provider = \Drupal::service('router.route_provider');
        $route = $route_provider->getRouteByName($route_name);
        $controller = $route->getDefaults()['_form'];

        $form = NULL;

        if (class_exists($controller)) {
          if ($parametrs == "null") {
            $form = new $controller();
            $form = \Drupal::formBuilder()->getForm($form);
          } else {
            if (Validator::isValidJson($parametrs)) {
              $form = new $controller($parametrs);
              $form = \Drupal::formBuilder()->getForm($form);
            }
          }

          if ($form) {
            $response->addCommand(new HtmlCommand('.modal-body', $form));
          }
        }
      }
    }

    return $response;
  }
}
