<?php

namespace Drupal\kvantstudio\Plugin\views\access;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Access plugin that provides access control for Scheduler.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "kvantstudio_taxonomy_permission",
 *   title = @Translation("Taxonomy permission"),
 *   help = @Translation("Access will be granted to users with the specified permission string.")
 * )
 */
final class TaxonomyPermission extends AccessPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = FALSE;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $entity_id = (int) $this->routeMatch->getRawParameter('taxonomy_term');
    if (!$entity_id) {
      return FALSE;
    }

    if ($account->hasPermission('administer taxonomy')) {
      return TRUE;
    }

    $entity = $this->entityTypeManager->getStorage('taxonomy_term')->load($entity_id);
    if ($entity) {
      return $account->hasPermission("view terms in {$entity->bundle()}");
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_custom_access', '\Drupal\kvantstudio\Access\TermAccessCheck::access');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user.permissions'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
