<?php

namespace Drupal\kvantstudio\Plugin\Field\FieldType;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Deriver for the kvantstudio_plugin_item field type.
 */
class PluginItemDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
