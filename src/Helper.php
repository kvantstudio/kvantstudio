<?php

namespace Drupal\kvantstudio;

use Drupal\Core\Database\Connection;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\Exception\FieldStorageDefinitionUpdateForbiddenException;
use Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\media\MediaInterface;

/**
 * Implements helper functions for interacting with the Drupal API.
 */
class Helper {

  use StringTranslationTrait;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The last installed schema repository.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected $entityLastInstalledSchemaRepository;

  /**
   * Base Database API class.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new Helper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager
   *   The entity definition update manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The last installed schema repository service.
   * @param \Drupal\Core\Database\Connection $connection
   *   Base Database API class.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager, FieldTypePluginManagerInterface $field_type_manager, EntityRepositoryInterface $entity_repository, EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository, Connection $connection) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->entityRepository = $entity_repository;
    $this->entityLastInstalledSchemaRepository = $entity_last_installed_schema_repository;
    $this->connection = $connection;
  }

  /**
   * List of available plugins.
   *
   * @param string $plugin_manager
   *
   * @return array
   */
  public function getPluginList(string $plugin_manager): array {
    $plugin_list = [];

    $definitions = \Drupal::service($plugin_manager)->getDefinitions();
    foreach ($definitions as $plugin_id => $plugin) {
      $plugin_list[$plugin_id] = $this->t($plugin['label']->render());
    }

    return $plugin_list;
  }

  /**
   * Gets default media entity for field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity for get default media entity from field.
   * @param string $field_name
   *   Field name.
   * @param bool $get_target_id
   *   If TRUE return ID of default media image.
   *
   * @return null|\Drupal\media\MediaInterface
   */
  public function getDefaultMedia(ContentEntityInterface $entity, string $field_name, bool $get_target_id = FALSE): ?MediaInterface {
    $media = NULL;
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name);
      $default_value = $field->getFieldDefinition()->getDefaultValue($entity);
      if ($default_value) {
        $target_id = (int) reset($default_value)['target_id'];
        if ($get_target_id) {
          return $target_id;
        }

        $media = $this->entityTypeManager->getStorage('media')->load($target_id);
      }
    }

    return $media;
  }

  /**
   * Sets default media value from field definition.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity for set default field media value.
   * @param string $field_name
   *   ID of field for get default media.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   */
  public function setDefaultMedia(ContentEntityInterface $entity, string $field_name): ContentEntityInterface {
    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name);
      $default_value = $field->getFieldDefinition()->getDefaultValue($entity);
      if ($default_value) {
        $target_id = (int) reset($default_value)['target_id'];
        $entity->set($field_name, [$target_id]);
        $entity->save();
      }
    }

    return $entity;
  }

  /**
   * Get the image uri for a file image field or default image uri (if exist).
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity with image field.
   * @param string $field_name
   *   ID of image media field from entity.
   * @param bool $generate_absolute_url
   *   If TRUE return absolute URL for image. If FALSE return image uri.
   * @param string $field_media_image
   *   ID of media type. Default 'field_media_image'.
   *
   * @return null|string
   */
  public function getMediaUri(ContentEntityInterface $entity, string $field_name, bool $generate_absolute_url = FALSE, string $field_media_image = 'field_media_image'): ?string {
    $image_uri = NULL;

    if ($entity->hasField($field_name)) {
      $field = $entity->get($field_name);

      if ($value = $field->getValue()) {
        $target_id = (int) reset($value)['target_id'];
        $file = File::load($target_id);
        if ($file) {
          $image_uri = $file->getFileUri();
        }
      }

      // If a set value above wasn't found, try the default image.
      if (!$image_uri) {
        // If image field is simple field.
        $default_image = $field->getSetting('default_image');
        if ($default_image) {
          if (!empty($default_image['uuid'])) {
            /** @var \Drupal\file\FileInterface $defaultImageFile */
            $defaultImageFile = $this->entityRepository->loadEntityByUuid('file', $default_image['uuid']);
            if ($defaultImageFile) {
              $image_uri = $defaultImageFile->getFileUri();
            }
          }
        } else {
          // If image field is media object.
          $value = $field->getFieldDefinition()->getDefaultValue($entity);
          if ($value) {
            $mid = (int) reset($value)['target_id'];
            $media = Media::load($mid);
            if ($media && $value = $media->get($field_media_image)->getValue()) {
              $target_id = (int) reset($value)['target_id'];
              $file = File::load($target_id);
              if ($file) {
                $image_uri = $file->getFileUri();
              }
            }
          }
        }
      }
    }

    if ($image_uri && $generate_absolute_url) {
      return \Drupal::service('file_url_generator')->generateAbsoluteString($image_uri);
    }

    return $image_uri;
  }

  /**
   * Helper function for install new entity type.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @return void
   */
  public function entityTypeInstall($entity_type_id): void {
    $original = $this->entityLastInstalledSchemaRepository->getLastInstalledDefinition($entity_type_id);
    if (!$original) {
      $this->entityTypeManager->clearCachedDefinitions();
      $definition = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($definition) {
        $this->entityDefinitionUpdateManager->installEntityType($definition);
      } else {
        throw new UnsupportedEntityTypeDefinitionException("Definition of '{$entity_type_id}' entity type not found.");
      }
    }
  }

  /**
   * Helper function for uninstall entity type.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @return void
   */
  public function entityTypeUninstall($entity_type_id): void {
    $original = $this->entityLastInstalledSchemaRepository->getLastInstalledDefinition($entity_type_id);
    if ($original) {
      $this->entityDefinitionUpdateManager->uninstallEntityType($original);
      $this->entityTypeManager->clearCachedDefinitions();
    }
  }

  /**
   * Helper function for create entity base field.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @param string $field_name_create
   *   Field name for creating.
   *
   * @return void
   */
  public function entityTypeBaseFieldCreate($entity_type_id, string $field_name_create): void {
    $base_field_definitions = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);
    $definition = $base_field_definitions[$field_name_create] ?? NULL;
    if (!$definition instanceof BaseFieldDefinition) {
      throw new FieldStorageDefinitionUpdateForbiddenException("Definition of field '{$field_name_create}' not found in '{$entity_type_id}' entity type.");
    }

    $type = $definition->getType();
    if ($type == 'entity_reference') {
      $definition->setInitialValue(0);
    }

    // Install the new definition.
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition($field_name_create, $entity_type_id, $definition->getProvider(), $definition);
  }

  /**
   * Helper function for delete entity base field.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @param string $field_name_delete
   *   Field name for deleting.
   *
   * @return void
   */
  public function entityTypeBaseFieldDelete($entity_type_id, string $field_name_delete): void {
    $field_storage_definition = $this->entityDefinitionUpdateManager->getFieldStorageDefinition($field_name_delete, $entity_type_id);
    if (!$field_storage_definition instanceof FieldStorageDefinitionInterface ) {
      throw new FieldStorageDefinitionUpdateForbiddenException("Definition of field '{$field_name_delete}' not found in '{$entity_type_id}' entity type.");
    }

    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definition);
  }

  /**
   * Helper function for rename entity base field.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @param string $field_name_delete
   *   Field name for deleting.
   * @param string $field_name_create
   *   Field name for creating.
   * @param bool $backup
   *   If TRUE will be created field_deleted_data_* table with all data rename field.
   *
   * @return void
   */
  public function entityTypeBaseFieldRename($entity_type_id, string $field_name_delete, string $field_name_create, bool $backup = TRUE): void {
    $transaction = $this->connection->startTransaction();

    /** @var \Drupal\Core\Entity\EntityTypeInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $bundle_definition = $this->entityTypeManager->getDefinition($entity_type_id);

    // Get data table name.
    $table_name = $storage->getDataTable() ?: $storage->getBaseTable();
    $id_key = $bundle_definition->getKey('id');

    // Create a new field definition.
    $base_field_definitions = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);
    $definition = $base_field_definitions[$field_name_create] ?? NULL;
    if (!$definition instanceof BaseFieldDefinition) {
      throw new FieldStorageDefinitionUpdateForbiddenException("Definition of field '{$field_name_create}' not found in '{$entity_type_id}' entity type.");
    }

    // Store the existing values.
    $existing_values = $this->connection->select($table_name)
      ->fields($table_name, [$id_key, $field_name_delete])
      ->execute()
      ->fetchAllKeyed();

    // Clear out the values.
    if (!$backup) {
      $this->connection->update($table_name)
      ->fields([$field_name_delete => NULL])
      ->execute();
    }

    // Uninstall the field.
    $field_storage_definition = $this->entityDefinitionUpdateManager->getFieldStorageDefinition($field_name_delete, $entity_type_id);
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definition);

    // Install the new definition.
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition($field_name_create, $entity_type_id, $definition->getProvider(), $definition);

    // Restore the values.
    foreach ($existing_values as $id => $value) {
      $this->connection->update($table_name)
        ->fields([$field_name_create => $value])
        ->condition($id_key, $id)
        ->execute();
    }

    unset($transaction);
  }

  /**
   * Helper function for delete entity field instance and storage.
   *
   * @param mixed $entity_type_id
   *   Entity type ID.
   * @param string $field_name
   *   Field name for deleting.
   * @param bool $delete_storage
   *   If TRUE storage will be deleted.
   *
   * @return void
   */
  public function entityTypeFieldDelete($entity_type_id, string $field_name, bool $delete_storage = TRUE): void {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
    foreach ($bundles as $key => $bundle) {
      if ($field_instance = FieldConfig::loadByName($entity_type_id, $key, $field_name)) {
        $field_instance->delete();
      }
    }

    if ($field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name)) {
      $field_storage->delete();
    }
  }

  /**
   * Helper function for add or remove columns from the field schema.
   *
   * @param string $field_type
   *   The field type id.
   * @param string $mode
   *   The mode 'add' or 'remove'.
   * @param array $columns
   *   Array of the column names from schema for add or remove.
   *
   * @return array $processed_fields
   */
  public function fieldTypeUpdate(string $field_type, string $mode, array $columns): array {
    $processed_fields = [];

    // Get class name where defined field type.
    $field_definition = $this->fieldTypeManager->getDefinition($field_type);
    $field_item_class = $field_definition['class'];

    // Returns a DatabaseSchema object for manipulating the schema.
    $schema = $this->connection->schema();

    // The key-value collection for tracking installed storage schema.
    $entity_storage_schema_sql = \Drupal::keyValue('entity.storage_schema.sql');
    $entity_definitions_installed = \Drupal::keyValue('entity.definitions.installed');

    // Gets a lightweight map of fields across bundles filtered by field type.
    $entity_field_map = $this->entityFieldManager->getFieldMapByFieldType($field_type);
    foreach ($entity_field_map as $entity_type_id => $field_map) {
      $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);

      // Only SQL storage based entities are supported / throw known exception.
      if (!($entity_storage instanceof SqlContentEntityStorage)) {
        continue;
      }

      // Gets the field storage definitions for a content entity type.
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);

      /** @var Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $entity_storage->getTableMapping($field_storage_definitions);

      /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition */
      foreach (array_intersect_key($field_storage_definitions, $field_map) as $field_storage_definition) {
        $field_name = $field_storage_definition->getName();
        $processed_fields[] = [$entity_type_id, $field_name];

        try {
          $table = $table_mapping->getFieldTableName($field_name);
        } catch (SqlContentEntityStorageException $e) {
          continue;
        }

        // See if the field has a revision table.
        $revision_table = NULL;
        if ($entity_type->isRevisionable() && $field_storage_definition->isRevisionable()) {
          if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
            $revision_table = $table_mapping->getDedicatedRevisionTableName($field_storage_definition);
          } elseif ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
            $revision_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
          }
        }

        // Load the installed field schema so that it can be updated.
        $schema_key = "$entity_type_id.field_schema_data.$field_name";
        $field_schema_data = $entity_storage_schema_sql->get($schema_key);

        // Current field schema.
        $field_schema = $field_item_class::schema($field_storage_definition);

        // For each add or remove column.
        foreach ($columns as $column_id) {
          $column = $table_mapping->getFieldColumnName($field_storage_definition, $column_id);

          if ($mode == 'add') {
            $spec = $field_schema['columns'][$column_id];

            // Add the new column.
            $field_exists = $schema->fieldExists($table, $column);
            if (!$field_exists) {
              $schema->addField($table, $column, $spec);
            }

            // Add the new column.
            if ($revision_table) {
              $field_exists = $schema->fieldExists($revision_table, $column);
              if (!$field_exists) {
                $schema->addField($revision_table, $column, $spec);
              }
            }

            // Add the new column to the installed field schema.
            if ($field_schema_data) {
              $field_schema_data[$table]['fields'][$column] = $spec;

              if ($revision_table) {
                $field_schema_data[$revision_table]['fields'][$column] = $spec;
              }
            }
          }

          if ($mode == 'remove') {
            $schema->dropField($table, $column);
            if ($revision_table) {
              $schema->dropField($revision_table, $column);
            }

            // Remove the column from the installed field schema.
            if ($field_schema_data) {
              unset($field_schema_data[$table]['fields'][$column]);

              if ($revision_table) {
                unset($field_schema_data[$revision_table]['fields'][$column]);
              }
            }
          }
        }

        // Save changes to the installed field schema.
        if ($field_schema_data) {
          $entity_storage_schema_sql->set($schema_key, $field_schema_data);
        }

        // Update field_storage_definitions.
        if ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
          $key = "$entity_type_id.field_storage_definitions";
          if ($definitions = $entity_definitions_installed->get($key)) {
            $definitions[$field_name] = $field_storage_definition;
            $entity_definitions_installed->set($key, $definitions);
          }
        }
      }

      // Update the field storage repository.
      $this->entityLastInstalledSchemaRepository->setLastInstalledFieldStorageDefinition($field_storage_definition);
    }

    return $processed_fields;
  }
}
