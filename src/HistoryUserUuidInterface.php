<?php

namespace Drupal\kvantstudio;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining HistoryUserUuid entities.
 *
 * @ingroup kvantstudio
 */
interface HistoryUserUuidInterface extends ContentEntityInterface, EntityOwnerInterface {

}
