<?php

namespace Drupal\kvantstudio;

use Drupal\Component\Utility\Crypt;

/**
 * Implements functions for generating tokens, passwords, etc.
 */
class Generator {

  /**
   * Generates a URL substitution token without special characters.
   * @param int $length
   *   Number of characters in the token after trimming.
   * @param int $length_max
   *   The maximum number of random characters in a token when generated.
   *   Must be at least 2 times the value of $length.
   */
  public function generateToken(int $length = 10, int $length_max = 60) {
    // Generates a token.
    $token = Crypt::randomBytesBase64($length_max);

    // Leave only letters and numbers in the token.
    $token = preg_replace('/[^A-Za-z0-9]/', '', $token);
    $token = substr($token, 0, $length);

    return $token;
  }

}
