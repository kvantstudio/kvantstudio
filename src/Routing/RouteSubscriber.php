<?php

namespace Drupal\kvantstudio\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();

    // This needs to run after that EntityResolverManager has set the route entity type.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -200];

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.admin_permissions')) {
      $route->setDefault('_form', '\Drupal\kvantstudio\Form\PermissionsForm');
    }

    if ($route = $collection->get('entity.user_role.edit_permissions_form')) {
      $route->setDefault('_form', '\Drupal\kvantstudio\Form\PermissionsRoleSpecificForm');
    }

    if ($route = $collection->get('entity.taxonomy_term.canonical')) {
      $route->setDefault('_controller', '\Drupal\kvantstudio\Controller\TermController::build');
    }
  }

}
