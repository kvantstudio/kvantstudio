<?php

namespace Drupal\kvantstudio;

use DeviceDetector\DeviceDetector;

/**
 * Defines the interface for device detect API.
 */
interface DeviceDetectorInterface {

  /**
   * Get DeviceDetector object.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $request;
   *   Response for a request.
   *
   * @return \DeviceDetector\DeviceDetector|null
   */
  public function getDeviceDetector($request = NULL): DeviceDetector|null;

  /**
   * Detect device information.
   *
   * @param bool $bot
   *   If bot are to be included in parsing.
   *
   * @return array
   *   Device information array.
   */
  public function detect(bool $bot = TRUE): array;

  /**
   * Detect device is bot.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $request;
   *   Response for a request.
   *
   * @return bool
   *   TRUE if device is bot.
   */
  public function isBot($request = NULL): bool;

  /**
   * Detect device is mobile.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $request;
   *   Response for a request.
   *
   * @return bool
   *   TRUE if device is mobile.
   */
  public function isMobile($request = NULL): bool;

}
