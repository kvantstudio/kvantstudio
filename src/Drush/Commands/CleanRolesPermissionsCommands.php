<?php

namespace Drupal\kvantstudio\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\PermissionHandlerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class CleanRolesPermissionsCommands extends DrushCommands {

  /**
   * Constructs a CleanRolesPermissionsCommands object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly PermissionHandlerInterface $permissionHandler,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('user.permissions'),
    );
  }

  /**
   * Removing access rights that were not created in the system from user roles.
   */
  #[CLI\Command(name: 'kvantstudio:clean-roles-permissions', aliases: ['kscp'])]
  #[CLI\Usage(name: 'kvantstudio:clean-roles-permissions kscp', description: 'Correction of the error. Removing access rights that were not created in the system from user roles.')]
  public function cleanRolesPermissions() {
    $permissions = array_keys($this->permissionHandler->getPermissions());

    /** @var \Drupal\user\Entity\Role[] $roles */
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $role) {
      $role_permissions = $role->getPermissions();
      $diff_permissions_in_role = array_diff($role_permissions, $permissions);
      if ($diff_permissions_in_role) {
        foreach ($diff_permissions_in_role as $permission) {
          $confirm = $this->confirm('Revoke ' . $permission . ' for ' . $role->id() . '?');
          if ($confirm) {
            $this->io()->note('Revoked');
            $role->revokePermission($permission);
          }
        }
        $role->save();
      }
    }

    $this->logger()->success(dt('Removing access rights completed.'));
  }

}
