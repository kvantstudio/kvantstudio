<?php

namespace Drupal\kvantstudio;

use Drupal\Component\Serialization\Json;

/**
 * Реализует функции для выполнения запросов к сторонним ресурсам.
 */
class Requests {

  /**
   * Возвращает текущий домен.
   */
  public function getHost() {
    $result = 'http://';

    // Проверка защищенного соединения.
    if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
      $result = 'https://';
    }

    // Название текущего домена. Например, site.com или www.site.com.
    $result .= $_SERVER['SERVER_NAME'];

    return $result;
  }

  /**
   * Отправляет GET или POST запрос используя cURL
   * @param string $url адрес запроса
   * @param string $type тип запроса get или post
   * @param bool $decode декодировать результат запроса если TRUE
   * @param array $data значения в виде массива передаваемые в запросе POST
   * @param array $options дополнительные параметры для cURL
   * @return string
   */
  public function curl(string $url, string $type = 'get', bool $decode = TRUE, array $data = [], array $options = []) {

    // Если url не является полноценным.
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
      $url = trim($url, '/');
      $url = $this->getHost() . '/' . $url;
    }

    $defaults = [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_FRESH_CONNECT => 1,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_ENCODING => "",
      CURLOPT_HEADER => 0,
    ];

    if ($type == 'get') {
      $defaults += [
        CURLOPT_CUSTOMREQUEST => "GET",
      ];
    }

    if ($type == 'post') {
      $result_data = [];
      $this->convertToStringArray($data, $result_data);

      $defaults += [
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $result_data,
      ];
    }

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    $response = curl_exec($ch);

    if ($response === FALSE) {
      $error = curl_error($ch);
      \Drupal::logger('kvantstudio')->error($error);
    }

    if ($response !== FALSE && $decode) {
      $response = Json::decode($response);
    }

    curl_close($ch);

    return $response;
  }

  /**
   * Преобразует многомерный массив в одномерный для передачи в CURL при POST запросах.
   */
  function convertToStringArray($inputArray, &$resultArray, $inputKey = '') {
    foreach ($inputArray as $key => $value) {
      $tmpKey = (bool) $inputKey ? $inputKey . "[$key]" : $key;
      if (is_array($value)) {
        $this->convertToStringArray($value, $resultArray, $tmpKey);
      } else {
        $resultArray[$tmpKey] = $value;
      }
    }
  }
}
