<?php

namespace Drupal\kvantstudio\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Test send mail form.
 */
class TestSendMailForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kvantstudio_test_send_mail_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $form['description'] = [
      '#markup' => '<p>' . $this->t('This page allows you to send a test e-mail to a recipient of your choice.') . '</p>',
    ];

    $form['test'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Recipient'),
      '#description' => '<p>' . $this->t('You can send a test e-mail to a recipient of your choice.') . '</p>',
    ];

    $form['test']['recipient'] = [
      '#title' => $this->t('E-mail'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => '',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data_mail = [
      'to' => trim($form_state->getValue(['test', 'recipient'])),
      'subject' => 'Test message',
      'message' => $this->t('An attempt has been made to send an e-mail to @email.', ['@email' => trim($form_state->getValue(['test', 'recipient']))]),
      'attachments' => [],
    ];
    $mail_delivery = kvantstudio_mail_send($data_mail);
    $message = '';
    if ($mail_delivery) {
      $message = $this->t('An attempt has been made to send an e-mail to @email.', ['@email' => trim($form_state->getValue(['test', 'recipient']))]);
    } else {
      $message = $this->t('There was a problem sending your message and it was not sent.');
    }
    \Drupal::messenger()->addMessage($message);
  }
}
