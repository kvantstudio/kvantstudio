<?php

namespace Drupal\kvantstudio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Kvantstudio settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kvantstudio_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'kvantstudio.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('kvantstudio.settings');

    $form['development'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Development'),
    ];

    $form['development']['use_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use debug function'),
      '#description' => $this->t('Allows using the kvantstudio_debug() procedural function or the debug method in the kvantstudio.debug service.'),
      '#default_value' => $config->get('use_debug'),
    ];

    $form['seo'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SEO'),
    ];

    $form['seo']['history_user_uuid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save the old user uuid'),
      '#default_value' => $config->get('history_user_uuid'),
    ];

    $form['federal_low'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings to suit site of the federal law on personal data'),
    ];

    // Номер node с соглашением об обработке персональных данных.
    $form['federal_low']['node_agreement_personal_data'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node number with terms of use'),
      '#default_value' => $config->get('node_agreement_personal_data'),
    ];

    // Номер node с политикой обработки персональных данных.
    $form['federal_low']['node_data_policy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node number with processing of personal data policy'),
      '#default_value' => $config->get('node_data_policy'),
    ];

    // Номер node с политикой использования файлов cookies.
    $form['federal_low']['node_cookie_policy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node number with policy use of cookie'),
      '#default_value' => $config->get('node_cookie_policy'),
    ];

    // Текст политики использования файлов cookies при первом посещении сайта.
    $form['federal_low']['text_cookie_policy'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The text of the policy of the use of cookie when you first visit the site'),
      '#default_value' => $config->get('text_cookie_policy'),
      '#rows' => 2,
    ];

    // Текст соглашения об обработке персональных данных для форм ввода данных.
    $form['federal_low']['text_data_policy'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The text of the agreement on personal data processing for data entry forms'),
      '#default_value' => $config->get('text_data_policy'),
      '#rows' => 2,
    ];

    $form['other'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Other settings'),
    ];

    // Номер node с соглашением об обработке персональных данных.
    $form['other']['canonical_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Сanonical domain'),
      '#default_value' => $config->get('canonical_host'),
      '#description' => $this->t('Only when the mode multisite. For example, http://www.example.com'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = $this->config('kvantstudio.settings');
    $config->set('use_debug', (bool) $form_state->getValue('use_debug'));
    $config->set('history_user_uuid', (bool) $form_state->getValue('history_user_uuid'));
    $config->set('canonical_host', trim($form_state->getValue('canonical_host')));
    $config->set('node_agreement_personal_data', trim($form_state->getValue('node_agreement_personal_data')));
    $config->set('node_data_policy', trim($form_state->getValue('node_data_policy')));
    $config->set('node_cookie_policy', trim($form_state->getValue('node_cookie_policy')));
    $config->set('text_cookie_policy', trim($form_state->getValue('text_cookie_policy')));
    $config->set('text_data_policy', trim($form_state->getValue('text_data_policy')));

    $config->save();
  }
}
