<?php

namespace Drupal\kvantstudio;

/**
 * Interface DebugInterface.
 */
Interface DebugInterface {

  /**
   * Logs information for debugging.
   *
   * This is a wrapper logging function which automatically decodes information for debugging.
   *
   * @param string $channel
   *   The name of the channel. Can be any string, but the general practice is to use the name of the subsystem calling this.
   * @param string $message
   *   The safe message to store in the log.
   * @param array|null $variables
   *   Array of variables to logs.
   * @param bool $is_json
   *   Type of encode variant. If TRUE $variables encoded in json.
   * @param bool $is_use_debug
   *   If TRUE $use_debug also TRUE.
   *
   * @return void
   */
  public function debug(string $channel, string $message, ?array $variables = [], bool $is_json = TRUE, bool $is_use_debug = FALSE): void;

}
