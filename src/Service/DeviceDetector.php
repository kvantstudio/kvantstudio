<?php

namespace Drupal\kvantstudio\Service;

use DeviceDetector\DeviceDetector as DeviceDetectorApi;
use Drupal\kvantstudio\DeviceDetectorInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class DeviceDetector.
 *
 * @package Drupal\kvantstudio\Services
 */
class DeviceDetector implements DeviceDetectorInterface {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * Static cache of device information. One per request.
   *
   * @var \SplObjectStorage
   */
  protected $detection;

  /**
   * Constructs a new DeviceDetector object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
    $this->detection = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function getDeviceDetector($request = NULL): DeviceDetectorApi|null {
    if (!$request instanceof RequestEvent) {
      $request = $this->request;
    }

    // Get user agent.
    $user_agent = $request->headers->get('User-Agent');

    // Initialize device detector.
    if ($user_agent) {
      $detect = new DeviceDetectorApi($user_agent);
      $detect->parse();

      return $detect;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function detect($bot = TRUE): array {
    if (!$this->detection->contains($this->request)) {
      // Get user agent.
      $user_agent = $this->request->headers->get('User-Agent');
      if (!$user_agent) {
        return [
          'type' => 'Device not defined'
        ];
      }

      // Initialize device detector.
      $detect = new DeviceDetectorApi($user_agent);
      $detect->parse();

      // Skip detection of bot, process as normal devices.
      if (!$bot) {
        $detect->skipBotDetection();
      }

      // Separate bot.
      if ($bot && $detect->isBot()) {
        $device['info'] = $detect->getBot();
        // Add generic type for bot.
        $device['type'] = 'bot';
      } else {
        // Get device type.
        // @see \DeviceDetector\Parser\Device\DeviceParserAbstract::$deviceTypes
        $device['type'] = $detect->getDeviceName();

        // Get device info.
        $device['info'] = [
          'client' => $detect->getClient(),
          'os' => $detect->getOs(),
          'brand' => $detect->getBrandName(),
          'model' => $detect->getModel(),
        ];
      }

      $this->detection[$this->request] = $device;
    }

    return $this->detection[$this->request];
  }

  /**
   * {@inheritdoc}
   */
  public function isBot($request = NULL): bool {
    if (!$request instanceof RequestEvent) {
      $request = $this->request;
    }

    // Get user agent.
    $user_agent = $request->headers->get('User-Agent');

    // Initialize device detector.
    if ($user_agent) {
      $detect = new DeviceDetectorApi($user_agent);
      $detect->parse();

      return $detect->isBot();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isMobile($request = NULL): bool {
    if (!$request instanceof RequestEvent) {
      $request = $this->request;
    }

    // Get user agent.
    $user_agent = $request->headers->get('User-Agent');

    // Initialize device detector.
    if ($user_agent) {
      $detect = new DeviceDetectorApi($user_agent);
      $detect->parse();

      $is_smartphone = $detect->isSmartphone();
      $is_tablet = $detect->isTablet();
      $is_phablet = $detect->isPhablet();

      if ($is_smartphone || $is_tablet || $is_phablet) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
