<?php

namespace Drupal\kvantstudio\Service;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\SessionManager;

/**
 * Session object.
 */
class Session {

  /**
   * @var \Drupal\Core\Session\SessionManager
   */
  protected $sessionManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   *   The current user account.
   */
  protected $currentUser;

  /**
   * Constructs a Session object.
   *
   * @param \Drupal\Core\Session\SessionManager $session_manager
   *   Manages user sessions.
   * @param \Drupal\Core\Session\AccountProxyInterface
   *   The current user account.
   */
  public function __construct(SessionManager $session_manager, AccountProxyInterface $current_user) {
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;
  }

}
