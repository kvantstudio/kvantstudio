<?php

namespace Drupal\kvantstudio;

use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

/**
 * Implements data formatting functions.
 */
class Formatter {

  /**
   * Форматирует отображение стоимости с разделителем.
   * Используется для вывода цены.
   *
   * @param mixed $value
   *   Значение цены.
   * @param int $decimals
   *   Количество знаков после разделителя.
   * @param string $dec_point
   *   Тип разделителя. По умолчанию точка.
   * @param string $thousands_sep
   *   Тип разделителя тысячной части.
   * @param bool $allow_zero
   *   Если TRUE будут выводиться незначащие нули.
   *
   * @return string
   */
  public static function price(mixed $value, int $decimals = 2, string $dec_point = '.', string $thousands_sep = ' ', bool $allow_zero = FALSE): string {
    // Проверка типа переменной.
    if (is_array($value) || is_object($value)) {
      return 0;
    }

    // Принудительно преобразуем тип к float.
    $value = (float) $value;

    // Форматирует число с разделением групп.
    $value = number_format($value, $decimals, $dec_point, $thousands_sep);

    // Возвращаем значение с незначащими нулями.
    if ($allow_zero) {
      return $value;
    }

    // Исключает из строкового значения незначащие нули.
    $value = trim($value, ' 0');
    $value = rtrim($value, '.');
    if (empty($value)) {
      $value = 0;
    }

    return (string) $value;
  }

  /**
   * Исключает из строкового значения незначащие нули после разделителя и возвращает десятичное число.
   * Используется при очистке строковых значений десятичных чисел, полученных из базы данных.
   *
   * @param mixed $value
   *   Строка с числовыми данными. Например "10.5200000000".
   *
   * @return float
   */
  public static function removeTrailingZeros(mixed $value): float {
    $value = (string) $value;
    $formatted_value = 0;

    if ($value) {
      $value = trim($value);
      preg_match("#^([\+\-]|)([0-9]*)(\.([0-9]*?)|)(0*)$#", $value, $o);
      $formatted_value = $o[1].sprintf('%d',$o[2]).($o[3]!='.'?$o[3]:'');
    }

    return (float) $formatted_value;
  }

  /**
   * Преобразовывает строку в десятичный вид заданной точности.
   * Используется для передачи чисел в базу данных.
   * Рекомендуется для цен использовать максимум 10 знаков после запятой
   * т.к. функция bccomp не обрабатывает более 10 знаков при считывании из базы данных.
   *
   * @param string $value
   *   Строка с числовыми данными. Например "10.5200000000".
   * @param int $decimals
   *   Количество знаков после разделителя.
   *
   * @return float
   */
  public static function decimal(string $value, int $decimals = 10): float {
    $value = (float) str_replace(',', '.', trim($value));
    return round($value, $decimals);
  }

  /**
   * Получить имя домена из URL.
   *
   * @param string $url
   *   Сторока URL для разбора.
   * @param bool $exclude_subdomains
   *   Если TRUE при проверке исключаются поддомены.
   *
   * @return string
   */
  public static function getDomainName(string $url, bool $exclude_subdomains = FALSE): string {
    $domain = parse_url($url, PHP_URL_HOST);
    if ($exclude_subdomains && substr_count($domain, '.') == 2) {
      $pos = (int) strpos($domain, '.') + 1;
      $result = substr($domain, $pos);
    } else {
      $result = $domain;
    }

    return (string) $result;
  }

  /**
   * Clearing a phone number from special characters.
   *
   * @param string|int|null $phone
   *   The phone number.
   *   If region is NULL, the phone number must be in MSISDN format.
   * @param string|null $region
   *   Region that we are expecting the number to be from.
   *   If it is not NULL, the verification for the correctness of the transmitted phone number will be performed.
   * @return int
   *   Validated and formatted phone number value.
   */
  public static function formatPhone(string|int|null $phone, ?string $region = null): int {
    // Clear the phone number of special characters.
    $phone = preg_replace('![^0-9]+!', '', (string) $phone);
    if (!$phone) {
      return 0;
    }

    $phone_formatted = 0;

    try {
      $phone_util = PhoneNumberUtil::getInstance();

      if ($region) {
        $phone_object = $phone_util->parse($phone, $region);
        if ($phone_util->isPossibleNumber($phone_object, $region)) {
          $phone_formatted = $phone_object->getNationalNumber();
        }
      } else {
        $phone_object = $phone_util->parse("+$phone");
        if ($phone_util->isPossibleNumber($phone_object)) {
          $phone_formatted = $phone_object->getNationalNumber();
        }
      }
    } catch (NumberParseException $e) {
      // No phone, no problem.
    }

    return (int) $phone_formatted;
  }

  /**
   * Gets the country code for the phone number.
   *
   * @param string|int|null $phone
   *   The phone number.
   *   If region is NULL, the phone number must be in MSISDN format.
   * @param string|null $region
   *   Region that we are expecting the number to be from.
   *   If it is not NULL, the verification for the correctness of the transmitted phone number will be performed.
   * @return int|bool
   *   The country code for the phone number.
   */
  public static function getCountryCodePhone(string|int|null$phone, ?string $region = null): int|bool {
    // Clear the phone number of special characters.
    $phone = preg_replace('![^0-9]+!', '', (string) $phone);
    if (!$phone) {
      return FALSE;
    }

    $code = NULL;

    try {
      $phone_util = PhoneNumberUtil::getInstance();

      if ($region) {
        $phone_object = $phone_util->parse($phone, $region);
        if ($phone_util->isPossibleNumber($phone_object, $region)) {
          $code = $phone_object->getCountryCode();
        }
      } else {
        $phone_object = $phone_util->parse("+$phone");
        if ($phone_util->isPossibleNumber($phone_object)) {
          $code = $phone_object->getCountryCode();
        }
      }
    } catch (NumberParseException $e) {
      // No phone, no problem.
    }

    if ($code) {
      return (int) $code;
    }

    return FALSE;
  }

  /**
   * Check if the phone number is valid.
   *
   * @param string|int|null $phone
   *   The phone number.
   *   If region is NULL, the phone number must be in MSISDN format.
   * @param string|null $region
   *   Region that we are expecting the number to be from.
   *   If it is not NULL, the verification for the correctness of the transmitted phone number will be performed.
   * @return bool
   *   TRUE if the phone number is valid.
   */
  public static function isPossiblePhone(string|int|null $phone, ?string $region = null): bool {
    // Clear the phone number of special characters.
    $phone = preg_replace('![^0-9]+!', '', (string) $phone);
    if (!$phone) {
      return FALSE;
    }

    try {
      $phone_util = PhoneNumberUtil::getInstance();

      if ($region) {
        $phone_object = $phone_util->parse($phone, $region);
        if ($phone_util->isPossibleNumber($phone_object, $region)) {
          return TRUE;
        }
      } else {
        $phone_object = $phone_util->parse("+$phone");
        if ($phone_util->isPossibleNumber($phone_object)) {
          return TRUE;
        }
      }
    } catch (NumberParseException $e) {
      // No phone, no problem.
    }

    return FALSE;
  }
}
