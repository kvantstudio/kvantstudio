<?php

declare(strict_types=1);

namespace Drupal\kvantstudio;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class for debug functions.
 */
class Debug implements DebugInterface {

  /**
   * The user settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * If TRUE debug allow.
   *
   * @var bool
   */
  protected $useDebug;

  /**
   * Constructs a new Debug.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger) {
    $this->config = $config_factory->get('kvantstudio.settings');
    $this->logger = $logger;

    $this->useDebug = (bool) $this->config->get('use_debug');
  }

  /**
   * {@inheritdoc}
   */
  public function debug(string $channel, string $message, ?array $variables = [], bool $is_json = TRUE, bool $is_use_debug = FALSE): void {
    $this->useDebug = $is_use_debug ?: $this->useDebug;
    if ($this->useDebug) {
      $context = [];

      $data = NULL;
      if ($variables) {
        if ($is_json) {
          $json = json_encode($variables, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
          $context = ['@json' => $json];
          $data = '@json';
        } else {
          $data = var_export($variables, TRUE);
        }
      }

      if ($data) {
        $message = "$message: <pre>$data</pre>";
      }

      $this->logger->get($channel)->debug($message, $context);
    }
  }
}
