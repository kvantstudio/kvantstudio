<?php

namespace Drupal\kvantstudio\Template;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\kvantstudio\Formatter;

/**
 * Twig extension adds a custom functions and a custom filters.
 */
class TwigExtension extends AbstractExtension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The alias manager that caches alias lookups based on the request.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The formatting functions object.
   *
   * @var \Drupal\kvantstudio\Formatter
   */
  protected $formatter;

  /**
   * Constructs a new TwigExtension.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\kvantstudio\Formatter $formatter
   *   The formatting functions object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, FileUrlGeneratorInterface $file_url_generator, Formatter $formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('kvantstudio.settings');
    $this->aliasManager = $alias_manager;
    $this->fileUrlGenerator = $file_url_generator;
    $this->formatter = $formatter;
  }

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return [
      new TwigFunction('kvantstudio_block', $this->block(...)),
      new TwigFunction('kvantstudio_entity_label', $this->entityLabel(...)),
      new TwigFunction('kvantstudio_entity_file_uri', [$this, 'entityFileUri']),
      new TwigFunction('kvantstudio_personal_data', $this->personalData(...)),
      new TwigFunction('kvantstudio_url_style_image', $this->urlStyleImage(...)),
      new TwigFunction('kvantstudio_format_price', $this->formatPrice(...)),
      new TwigFunction('kvantstudio_menu', $this->menu(...)),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'kvantstudio.twig_extension';
  }

  /**
   * Формирует содержимое блока по идентификатору.
   *
   *
   * @param string $id - идентификатор блока.
   * @param string $entity_type_id
   *   The entity type ID for this storage. block - для блоков созданных программно, block_content - для кастомных блоков.
   *
   * @return array
   *   A render array for the entity.
   */
  public function block($id, $entity_type_id = 'block_content') {
    $block_view = '';

    $block = $this->entityTypeManager->getStorage($entity_type_id)->load($id);
    if ($block) {
      $block_view = $this->entityTypeManager->getViewBuilder($entity_type_id)->view($block);
    }

    return $block_view;
  }

  /**
   * Формирует URL ссылки для соглашения и политики по обработке персональных данных.
   * TODO: требуется доработка вывода через шаблон ссылок на соглашение и политику.
   */
  public function personalData($type = 'cookie', $label = "Take COOKIE") {
    $alias = "";
    $data = "";

    if ($type == 'agreement') {
      $nid = $this->config->get('node_agreement_personal_data');
      if ($nid) {
        if (is_numeric($nid)) {
          $alias = $this->aliasManager->getAliasByPath('/node/' . $nid);
        } else {
          $uri = 'public://' . $nid;
          $alias = $this->fileUrlGenerator->generateAbsoluteString($uri);
        }
      }
      if ($alias) {
        $data = '<a class="personal-data personal-data__agreement" target="_blank" rel="nofollow noopener" href="' . $alias . '">' . $label . '</a>';
      }
    }

    if ($type == 'policy') {
      $nid = $this->config->get('node_data_policy');
      if ($nid) {
        if (is_numeric($nid)) {
          $alias = $this->aliasManager->getAliasByPath('/node/' . $nid);
        } else {
          $uri = 'public://' . $nid;
          $alias = $this->fileUrlGenerator->generateAbsoluteString($uri);
        }
      }
      if ($alias) {
        $data = '<a class="personal-data personal-data__policy" target="_blank" rel="nofollow noopener" href="' . $alias . '">' . $label . '</a>';
      }
    }

    if ($type == 'cookie') {
      $data = [
        '#theme' => 'kvantstudio_personal_data_confirm',
        '#label' => $label,
        '#attached' => [
          'library' => [
            'kvantstudio/personal_data_confirm',
          ],
        ],
      ];
    }

    return $data;
  }

  /**
   * Формирует URL изображения по заданному стилю и хранилищу.
   */
  public function urlStyleImage($image_style, $path, $storage = 'public') {
    $url = '';
    $style = ImageStyle::load($image_style);
    if ($style) {
      $uri = $storage . '://' . $path;
      $url = $style->buildUrl($uri);
    }

    return $url;
  }

  /**
   * Returns the entity label.
   *
   * @param string $entity_type
   *   The entity type.
   * @param mixed $id
   *   The ID of the entity to build.
   * @param string $langcode
   *   (optional) For which language the entity should be rendered, defaults to
   *   the current content language.
   * @param bool $check_access
   *   (optional) Indicates that access check is required.
   *
   * @return null|string
   *   A label for the entity or NULL if the entity does not exist.
   */
  public function entityLabel(string $entity_type, int $id, string $langcode = NULL, bool $check_access = TRUE) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($id);
    if ($entity instanceof ContentEntityInterface) {
      $access = $check_access ? $entity->access('view', NULL, TRUE) : AccessResult::allowed();
      if ($access->isAllowed()) {
        if ($langcode && $entity->hasTranslation($langcode)) {
          $entity = $entity->getTranslation($langcode);
        }
        return $entity->label();
      }
    }
  }

  /**
   * Returns the array of uri entity file field.
   *
   * @param string $field_name
   *   The field name.
   * @param string $entity_type
   *   The entity type.
   * @param mixed $id
   *   The ID of the entity to render.
   * @param string $langcode
   *   (optional) Language code to load translation.
   * @param bool $check_access
   *   (optional) Indicates that access check is required.
   *
   * @return null|array
   *   A render array for the field or NULL if the value does not exist.
   */
  public function fileUri($field_name, $entity_type, $id = NULL, $langcode = NULL, $check_access = TRUE) {
    $data = [];

    $entity = NULL;
    if ($id) {
      $entity = $this->entityTypeManager->getStorage($entity_type)->load($id);
    }

    if ($entity) {
      $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity, $langcode);
      $access = $check_access ? $entity->access('view', NULL, TRUE) : AccessResult::allowed();
      if ($access->isAllowed()) {
        if (isset($entity->{$field_name})) {
          $media_entities = $entity->{$field_name}->getValue();
          foreach ($media_entities as $key => $value) {
            $target_id = $value['target_id'];
            $media = Media::load($target_id);
            if ($media instanceof MediaInterface) {
              $source = $media->getSource();
              $target_id = $source->getSourceFieldValue($media);
            }
            if ($file = File::load($target_id)) {
              $data[] = $file->getFileUri();
            }
          }
        }
      }
    }

    return $data;
  }

  /**
   * Форматирует стоимость в виде строки через разделитель пробел для тысячных знаков.
   * Используется для вывода на страницу цен.
   *
   * @param float $value
   *   Cтоимость.
   * @param int $decimals
   *   Устанавливает число знаков после запятой.
   * @param string $dec_point
   *   Устанавливает разделитель дробной части.
   * @param string $thousands_sep
   *   Устанавливает разделитель тысяч.
   * @param bool $allow_zero
   *   Если TRUE, разрешаем отображать незначащие нули.
   *
   * @return string
   *   Форматированная строка.
   */
  public function formatPrice($value, $decimals = 2, $dec_point = ".", $thousands_sep = " ", $allow_zero = FALSE) {
    return $this->formatter->price($value, $decimals, $dec_point, $thousands_sep, $allow_zero);
  }

  /**
   * Provides function to programmatically rendering a menu.
   *
   * @param string $menu_name
   *   The machine configuration id of the menu to render.
   */
  public function menu($menu_name) {
    $menu_tree = \Drupal::menuTree();

    // Build the typical default set of menu tree parameters.
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);

    // Load the tree based on this set of parameters.
    $tree = $menu_tree->load($menu_name, $parameters);

    // Transform the tree using the manipulators you want.
    $manipulators = [
      // Only show links that are accessible for the current user.
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      // Use the default sorting of menu links.
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menu_tree->transform($tree, $manipulators);

    // Finally, build a renderable array from the transformed tree.
    $menu = $menu_tree->build($tree);
    $menu['#attributes']['class'] = 'menu menu__' . $menu_name;

    return $menu;
  }
}
