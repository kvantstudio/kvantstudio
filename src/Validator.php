<?php

namespace Drupal\kvantstudio;

use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\filehash\FileHash;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\file\FileUsage\FileUsageInterface;

/**
 * Реализует функции валидации при работе с данными.
 */
class Validator {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Provides the File Hash service.
   *
   * @var \Drupal\filehash\FileHash
   */
  protected $fileHash;

  /**
   * Constructs a new Validator object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   *   The file usage service.
   * @param \Drupal\filehash\FileHash $file_hash
   *   Provides the File Hash service.
   */
  public function __construct(AccountProxyInterface $current_user, EntityFieldManagerInterface $entity_field_manager, FileUsageInterface $file_usage, FileHash $file_hash) {
    $this->account = $current_user;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileUsage = $file_usage;
    $this->fileHash = $file_hash;
  }

  /**
   * Сравнение двух чисел произвольной точности.
   * По умолчанию сравниваем число с нулем.
   *
   * @param mixed $parametr1
   *   Первое число.
   * @param mixed $parametr2
   *   Второе число, с которым сравнивается первое.
   * @param bool $return_bool
   *   Если TRUE, будет возвращен результат сравнения первого числа со вторым.
   * @param integer|null $scale
   *   Точность сравнения. По умолчанию до 10 знаков. Максимальное значение 10.
   * @return bool|integer
   *   TRUE будет означать, что $parametr1 > $parametr2 если $return_bool = TRUE.
   *   Если $return_bool = FALSE, то будет возвращено числовое значение функции bccomp - 0 if the two operands are equal, 1 if the left_operand is larger than the right_operand, -1 otherwise.
   */
  public static function comparingNumbers($parametr1, $parametr2 = 0, bool $return_bool = TRUE, ?int $scale = 10) {
    $num1 = (string) sprintf('%.10f', $parametr1);
    $num2 = (string) sprintf('%.10f', $parametr2);
    $result = bccomp($num1, $num2, $scale);

    if ($return_bool) {
      // 0 if the $result are equal 1.
      if (!($result <=> 1)) {
        return TRUE;
      }

      return FALSE;
    }

    return $result;
  }

  /**
   * Проверяет является ли пользователь владельцем сущности.
   *
   * @param \Drupal\user\EntityOwnerInterface|Drupal\Core\Entity\ContentEntityInterface $entity
   * @return bool
   */
  function isUserHasAccessOwnerEntity($entity) {
    if ($this->account->isAuthenticated()) {
      $uid = 0;

      if ($entity instanceof EntityOwnerInterface) {
        $uid = $entity->getOwnerId();
      } else {
        if ($entity instanceof ContentEntityInterface && $entity->hasField('uid')) {
          $uid = $entity->get('uid')->first()->getValue();
        }
      }

      if ($uid == $this->account->id()) {
        return TRUE;
      }
    } else {
      if ($entity instanceof ContentEntityInterface && $entity->hasField('drupal_uuid')) {
        $drupal_connect_uuid = kvantstudio_user_hash();
        $entity_connect_uuid = $entity->get('drupal_uuid')->first()->getValue()['value'];
        if ($drupal_connect_uuid == $entity_connect_uuid) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Проверяет наличие поля у типа сущности.
   *
   * @param string $entity_type
   * @param string $bundle
   * @param string $field_name
   * @return bool
   */
  function doesBundleHaveField(string $entity_type, string $bundle, string $field_name) {
    $all_bundle_fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    return isset($all_bundle_fields[$field_name]);
  }

  /**
   * Checks a string for valid data in json format.
   *
   * @param mixed $string
   *   String in json format.
   * @return bool
   *   TRUE if string has valid json format.
   */
  public static function isValidJson(mixed $string): bool {
    if (is_string($string)) {
      Json::decode($string);
      $error = json_last_error();
      if ($error === JSON_ERROR_NONE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Decode string in json format.
   *
   * @param mixed $string
   *   String in json format.
   * @return array|bool
   *   Array if string has valid json format.
   */
  public static function jsonDecode(mixed $string): array|bool {
    if (is_string($string)) {
      $result = Json::decode($string);
      $error = json_last_error();
      if ($error === JSON_ERROR_NONE) {
        return (array) $result;
      }
    }

    return FALSE;
  }

  /**
   * Checks that file has a duplicate.
   *
   * @param FileInterface $file
   * @return \Drupal\file\FileInterface
   */
  function fileHasDuplicate(FileInterface $file) {
    $file_exist = NULL;

    $param = 'has_dublicate';
    $file->{$param} = FALSE;

    $fid = 0;
    foreach ($this->fileHash->getEnabledAlgorithms() as $column) {
      try {
        $fid = $this->fileHash->duplicateLookup($column, $file);
      } catch (DatabaseExceptionWrapper $e) {
        $this->fileHash->addColumns();
        $fid = $this->fileHash->duplicateLookup($column, $file);
      }

      if ($fid && $fid != $file->id()) {
        $file_exist = File::load($fid);
        if ($file_exist instanceof FileInterface) {
          break;
        }
      }
    }

    // Если был найден ранее загруженный файл.
    if ($file_exist instanceof FileInterface) {
      // Помечаем ранее загруженный файл постоянным если проверяемый файл постоянный,
      // а ранее загруженный файл помечен на удаление.
      if (!$file_exist->isPermanent() && $file->isPermanent()) {
        $file_exist->setPermanent();
        $file_exist->save();
      }

      // Удаляем проверяемый файл, если он нигде не используется.
      $references = $this->fileUsage->listUsage($file);
      if (empty($references)) {
        try {
          $file->delete();
        } catch (FileException $e) {
          // Ignore and continue.
        }
      }

      $file_exist->{$param} = TRUE;

      return $file_exist;
    }

    return $file;
  }

  /**
   * Check if a given string is a valid UUID
   *
   * @param mixed $uuid
   *   The value to check.
   * @return boolean
   */
  public static function isValidUuid($uuid): bool {
    if (is_string($uuid) && preg_match('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $uuid)) {
      return TRUE;
    }

    return FALSE;
  }
}
