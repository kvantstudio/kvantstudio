<?php

namespace Drupal\kvantstudio\Event;

/**
 * Provides list of event names.
 */
abstract class TermEvents {

  /**
   * Event which called for building render array for term controller.
   *
   * @Event
   *
   * @see \Drupal\kvantstudio\Event\TermPageBuildEvent
   * @see \Drupal\kvantstudio\Controller\TaxonomyController
   */
  const PAGE_BUILD = 'kvantstudio.term_page_build';

}
