<?php

namespace Drupal\kvantstudio\Event;

use Drupal\taxonomy\TermInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides event which fires when prepare term page render array result.
 */
class TermPageBuildEvent extends Event {

  /**
   * The render array for page.
   *
   * @var array
   */
  protected $build = [];

  /**
   * The taxonomy term.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $entity;

  /**
   * Constructs a new TermPageBuildEvent object.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy_term
   *   The taxonomy term.
   */
  public function __construct(TermInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Sets build array as result for page.
   *
   * @param array $build
   *   The render array.
   */
  public function setBuildArray(array $build): void {
    $this->build = $build;
  }

  /**
   * Gets build result for the page.
   *
   * @return array
   *   The render array.
   */
  public function getBuildArray(): array {
    return $this->build;
  }

  /**
   * Gets taxonomy term entity.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The taxonomy term.
   */
  public function getEntity(): TermInterface {
    return $this->entity;
  }

}
