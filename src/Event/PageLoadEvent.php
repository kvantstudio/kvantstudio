<?php

namespace Drupal\kvantstudio\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Events while page loading.
 */
class PageLoadEvent extends Event {

  /**
   * Event in js window.onload.
   */
  const PAGE_LOAD = 'kvantstudio.page.load';

  /**
   * @param Request $request
   *   An HTTP request.
   */
  protected $request;

  /**
   * @param AjaxResponse $response
   *   JSON response object.
   */
  protected $response;

  /**
   * @param array $data
   *   Data for results.
   */
  protected $data;

  /**
   * PageLoadEvent constructor.
   */
  public function __construct(Request $request, AjaxResponse $response, array $data) {
    $this->request = $request;
    $this->response = $response;
    $this->data = $data;
  }

  /**
   * Gets HTTP request.
   */
  public function getRequest(): Request {
    return $this->request;
  }

  /**
   * Gets response object.
   */
  public function getResponse(): AjaxResponse {
    return $this->response;
  }

  /**
   * Gets data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Sets HTTP request.
   */
  public function setRequest(Request $request): PageLoadEvent {
    $this->request = $request;
    return $this;
  }

  /**
   * Sets response object.
   */
  public function setResponse(AjaxResponse $response): PageLoadEvent {
    $this->response = $response;
    return $this;
  }

  /**
   * Gets data.
   */
  public function setData(array $data): PageLoadEvent {
    $this->data = $data;
    return $this;
  }
}
