<?php

namespace Drupal\kvantstudio\Event;

use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Events while change user UUID.
 */
class ChangeUserUuidEvent extends Event {

  /**
   * Event if need change user UUID.
   */
  const CHANGE_USER_UUID = 'kvantstudio.user.change_uuid';

  /**
   * @param UserInterface $account
   *   User entity.
   */
  protected $account;

  /**
   * @param string $uuid
   *   The old user UUID value from COOKIES.
   */
  protected $uuid;

  /**
   * ChangeUserUuidEvent constructor.
   */
  public function __construct(UserInterface $account, string $uuid) {
    $this->account = $account;
    $this->uuid = $uuid;
  }

  /**
   * Gets user entity.
   */
  public function getAccount(): UserInterface {
    return $this->account;
  }

  /**
   * Gets old user uuid.
   */
  public function getUuid(): string {
    return $this->uuid;
  }

  /**
   * Sets user entity.
   */
  public function setAccount(UserInterface $account): ChangeUserUuidEvent {
    $this->account = $account;
    return $this;
  }
}
