<?php

namespace Drupal\kvantstudio\EventSubscriber;

use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\kvantstudio\Event\ChangeUserUuidEvent;
use Drupal\kvantstudio\Event\PageLoadEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber ConnectUUID.
 */
class ConnectUUIDEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current request
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Constructs a ConfigEntityStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack object.
   * @param \Drupal\Core\Session\AccountProxyInterface
   *   The current user account.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request_stack->getCurrentRequest();
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    // Run before DynamicPageCacheSubscriber::onResponse, which has priority 100.
    $events[KernelEvents::RESPONSE][] = ['onResponse', 101];
    $events[PageLoadEvent::PAGE_LOAD][] = ['onPageLoad'];
    $events[ChangeUserUuidEvent::CHANGE_USER_UUID][] = ['onChangeUserUuid'];

    return $events;
  }

  /**
   * Stores a response in case of a ConnectUUID.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   * @return void
   */
  public function onResponse(ResponseEvent $event): void {
    $request = $event->getRequest();
    $drupal_connect_uuid = $request->cookies->has('drupal_connect_uuid');
    if (!$drupal_connect_uuid) {
      $response = $event->getResponse();
      $drupal_connect_uuid = $this->uuidService->generate();
      if ($this->currentUser->isAuthenticated()) {
        /** @var \Drupal\user\Entity\User $account */
        $account = User::load($this->currentUser->id());
        $drupal_connect_uuid = $account->uuid();
      }

      $cookie = Cookie::create('drupal_connect_uuid', $drupal_connect_uuid, 0, '/', NULL, FALSE, FALSE, FALSE, NULL);
      $response->headers->setCookie($cookie);
    }
  }

  /**
   * On page load event.
   *
   * @param \Drupal\kvantstudio\Event\PageLoadEvent $event
   *   The event to process.
   * @return void
   */
  public function onPageLoad(PageLoadEvent $event): void {
    $request = $event->getRequest();

    $response = $event->getResponse();

    $drupal_connect_uuid = NULL;
    if ($this->currentUser->isAuthenticated()) {
      /** @var \Drupal\user\Entity\User $account */
      $account = User::load($this->currentUser->id());
      $drupal_connect_uuid = $account->uuid();
    }

    if ($drupal_connect_uuid) {
      $cookie = Cookie::create('drupal_connect_uuid', $drupal_connect_uuid, 0, '/', NULL, FALSE, FALSE, FALSE, NULL);
      $response->headers->setCookie($cookie);
      $event->setResponse($response);
    }
  }

  /**
   * Events while change user UUID.
   *
   * @param Drupal\kvantstudio\Event\ChangeUserUuidEvent $event
   * @return void
   */
  public function onChangeUserUuid(ChangeUserUuidEvent $event) {
    $settings = $this->configFactory->get('kvantstudio.settings');
    if ((bool) $settings->get('history_user_uuid')) {
      $account = $event->getAccount();
      $uuid = $event->getUuid();

      // Save the old user temp uuid.
      $storage = $this->entityTypeManager->getStorage('history_user_uuid');
      $history_user_uuid = $storage->create([
        'uid' => $account->id(),
        'user_uuid' => $uuid,
        'google_uuid' => kvantstudio_get_google_analytics_client_id(),
        'yandex_uuid' => kvantstudio_get_yandex_metrica_client_id(),
        'hostname' => $this->request->getClientIp()
      ]);
      $history_user_uuid->save();
    }
  }
}
