<?php

namespace Drupal\kvantstudio\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\kvantstudio\HistoryUserUuidInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the HistoryUserUuid entity.
 *
 * @ingroup kvantstudio
 *
 * @ContentEntityType(
 *   id = "history_user_uuid",
 *   label = @Translation("History user UUID"),
 *   base_table = "history_user_uuid",
 *   admin_permission = "administer history_user_uuid",
 *   entity_keys = {
 *     "id" = "huuid",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   }
 * )
 */
class HistoryUserUuid extends ContentEntityBase implements HistoryUserUuidInterface {

  use StringTranslationTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('User ID'));

    $fields['user_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The old user UUID'))
      ->setSetting('max_length', 255)
      ->setRequired(TRUE);

    $fields['google_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Google UUID'))
      ->setSetting('max_length', 255)
      ->setRequired(FALSE);

    $fields['yandex_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Yandex UUID'))
      ->setSetting('max_length', 255)
      ->setRequired(FALSE);

    $fields['hostname'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP address'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the item was created.'));

    return $fields;
  }
}
