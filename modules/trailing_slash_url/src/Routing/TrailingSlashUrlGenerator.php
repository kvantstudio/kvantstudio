<?php

namespace Drupal\trailing_slash_url\Routing;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Routing\UrlGenerator;
use Drupal\trailing_slash_url\Helper\Url\TrailingSlashUrlHelper;
use Drupal\language\HttpKernel\PathProcessorLanguage;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Generates URLs from route names and parameters.
 *
 * We extend the Urls generation service to add trailing slash
 * to the Urls that correspond to <front> in multilanguage
 * that by default is removed after the processOutbound.
 */
class TrailingSlashUrlGenerator extends UrlGenerator {

  /**
   * @var \Drupal\language\HttpKernel\PathProcessorLanguage
   */
  private $pathProcessorLanguage;

  /**
   * Constructs a new generator object.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $provider
   *   The route provider to be searched for routes.
   * @param \Drupal\Core\PathProcessor\OutboundPathProcessorInterface $path_processor
   *   The path processor to convert the system path to one suitable for URLs.
   * @param \Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface $route_processor
   *   The route processor.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack object.
   * @param Drupal\language\HttpKernel\PathProcessorLanguage $path_processor_language
   *   Processes the inbound path using path alias lookups.
   * @param string[] $filter_protocols
   *   An array of protocols allowed for URL generation.
   */
  public function __construct(RouteProviderInterface $provider, OutboundPathProcessorInterface $path_processor, OutboundRouteProcessorInterface $route_processor, RequestStack $request_stack, array $filter_protocols, PathProcessorLanguage $path_processor_language) {
    if (empty($filter_protocols)) {
      $filter_protocols = ['http', 'https'];
    }
    parent::__construct($provider, $path_processor, $route_processor, $request_stack, $filter_protocols);
    $this->pathProcessorLanguage = $path_processor_language;
  }

  /**
   * {@inheritdoc}
   */
  public function generateFromRoute($name, $parameters = [], $options = [], $collect_bubbleable_metadata = FALSE) {
    $url = parent::generateFromRoute($name, $parameters, $options, $collect_bubbleable_metadata);

    if (\Drupal::languageManager()->isMultilingual() && $name === '<front>' && $this->getLanguagePrefix($name, $options)) {
      if ($collect_bubbleable_metadata) {
        $url->setGeneratedUrl($this->fixFrontMultilingualUrl($url->getGeneratedUrl()));
      }
      else {
        $url = $this->fixFrontMultilingualUrl($url);
      }
    }

    return $url;
  }

  /**
   * @param $route_name
   * @param $options
   *
   * @return string
   */
  private function getLanguagePrefix($route_name, $options) {
    $language_options = $options;
    $this->pathProcessorLanguage->processOutbound($route_name, $language_options, $this->requestStack->getCurrentRequest());

    return empty($language_options['prefix']) ? '' : $language_options['prefix'];
  }

  /**
   * @param $url
   *
   * @return string
   */
  private function fixFrontMultilingualUrl($url) {
    TrailingSlashUrlHelper::add($url);

    return $url;
  }

}
