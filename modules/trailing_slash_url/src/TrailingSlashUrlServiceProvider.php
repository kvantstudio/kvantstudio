<?php

namespace Drupal\trailing_slash_url;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class TrailingSlashUrlServiceProvider.
 *
 * @package Drupal\trailing_slash_url
 */
class TrailingSlashUrlServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    if ($container->hasDefinition('url_generator.non_bubbling') && $container->hasDefinition('path_processor_language')) {
      $container->getDefinition('url_generator.non_bubbling')
        ->setClass('Drupal\trailing_slash_url\Routing\TrailingSlashUrlGenerator')
        ->addArgument(new Reference('path_processor_language'));
    }
  }

}
