<?php

namespace Drupal\trailing_slash_url\Helper\Url;

/**
 * Class TrailingSlashHelper
 *
 * @package Drupal\trailing_slash_url\Helper\Url
 */
class TrailingSlashUrlHelper {

  /**
   * Add a slash at the end if you do not have one.
   *
   * @param $path
   */
  public static function add(&$path) {
    $path = preg_replace('/((?:^|\\/)[^\\/\\.]+?)$/isD', '$1/', $path);
  }

}
