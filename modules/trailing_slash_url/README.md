# TRAILING SLASH URL DRUPAL MODULE

## What is it?

Adds trailing slashes to all URLs you want.
For example: example.com/user/.
This feature could be usefull for SEO motivations.
